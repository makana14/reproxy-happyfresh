var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var rulesRouter = require('./routes/rules');
var hfAutomationV2Router = require('./routes/hf-automation-v2');

var fs = require('fs')
var https = require('https')
var http = require('http')
var cors = require('cors');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.disable('etag');

app.use('/', indexRouter);
app.use('/rules', rulesRouter);
app.use('/hf-automation-v2', hfAutomationV2Router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.set('statusFeatureEnabled', true);

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

http.createServer(app)
.listen(3443, function () {
  console.log('HTTPS server running on port 3443! Start hitting https://localhost:3443/rules/run/:yourRuleSetName')
});

module.exports = app;
