var express = require('express');
var router = express.Router();
var hfAutomationController = require('../controllers/hfAutomationV2Controller');

/** For TE automation pilot project */
router.all('/modify/reset/:ruleSetName', async (req, res, next) => {
  await hfAutomationController.reset(req, res, req.params.ruleSetName);
});

router.post('/modify/resetBulk', async (req, res, next) => {
  await hfAutomationController.resetBulk(req, res);
});

router.post('/modify/changeBehavior/:ruleSetName', async (req, res, next) => {
  await hfAutomationController.changeBehavior(req, res, req.params.ruleSetName);
});

router.post('/session/create/:automationFolder/:sessionId', async (req, res, next) => {
  await hfAutomationController.createSession(req, res, req.params.automationFolder, req.params.sessionId);
});

router.post('/session/end/:sessionId', async (req, res, next) => {
  await hfAutomationController.endSession(req, res, req.params.sessionId);
});

module.exports = router;
