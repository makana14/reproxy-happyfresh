var express = require('express');
var router = express.Router();
var rulesRunnerController = require('../controllers/rulesRunnerController');
var hfAutomationController = require('../controllers/oldAutomationController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send("Edit the URL to: rules/run/{yourRulesName}/{actualURL}");
});

/** The function to run the ruleSet */
router.all('/run/:rulesName/*', async (req, res, next) => {
  await rulesRunnerController.getResponse(req, res, req.params.rulesName);
});


/** For SDET automation pilot project */
router.all('/hf-automation/modify/reset/:ruleSetName', async (req, res, next) => {
  await hfAutomationController.reset(req, res, req.params.ruleSetName);
});

router.post('/hf-automation/modify/resetBulk', async (req, res, next) => {
  await hfAutomationController.resetBulk(req, res);
});

router.post('/hf-automation/modify/changeBehavior/:ruleSetName', async (req, res, next) => {
  await hfAutomationController.changeBehavior(req, res, req.params.ruleSetName);
});

router.post('/hf-automation/session/create/happyfresh/:sessionId', async (req, res, next) => {
  await hfAutomationController.createSession(req, res, "happyfresh.automation", req.params.sessionId);
});

router.post('/hf-automation/session/end/:sessionId', async (req, res, next) => {
  await hfAutomationController.endSession(req, res, req.params.sessionId);
});

module.exports = router;
