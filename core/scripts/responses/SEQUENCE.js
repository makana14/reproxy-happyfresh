const responseCreator = require("../../../controllers/util/ruleResponseCreator")

async function execute(request, response, arg, logs) {
    for (const responseSpec of arg) {
        response = await responseCreator.createResponse(request, responseSpec, logs, response);
    }

    return response;

}

module.exports = execute;