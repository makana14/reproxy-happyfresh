async function execute(request, response, arg, logs) {
    response.data = {
        errorType: "SERVER_ERROR",
        userErrorMessage: "Unexpected server error occurred. We apologize for the inconvenience. Please try again later.",
        errorMessage: "Uncaught exception: java.lang.Exception: excpetion occured while serving the page content"
    };
    response.status = 500;
    return response;
}

module.exports = execute;
