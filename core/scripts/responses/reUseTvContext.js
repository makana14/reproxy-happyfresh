async function execute(request, response, arg) {
    if (!request.statusFeatureEnabled) {
        if (request.body.context != null) {
            response.context = request.body.context;
        }
        return response;
    } else {
        if (!isNaN(response.status) && response.status % 100 <= 3) {
            if (request.body.context != null) {
                response.data.context = request.body.context;
            }
        }

        return response;
    }
}

module.exports = execute;