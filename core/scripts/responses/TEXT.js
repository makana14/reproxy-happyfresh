async function execute(request, response, arg, logs) {
    if (!request.statusFeatureEnabled) {
        return nonStatusExecute(request, response, arg, logs);
    }

    response.data = arg;
    return response;
}

function nonStatusExecute(request, response, arg, logs) {
    return arg;
}

module.exports = execute;
