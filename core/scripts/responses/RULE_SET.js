const ruleSetResponseCreator = require('../../../controllers/util/ruleSetResponseCreator')

async function execute(request, response, arg, logs) {
    return await ruleSetResponseCreator.createResponseFromRuleSetDirectory(request, arg, logs, response);
}

module.exports = execute;