const jsonFileResponseScript = require('./JSON_FILE')

async function execute(request, response, arg, logs) {
    let path = request.urlObj.pathname;
    let behaviorFileName = "default.json";

    if (arg != null) {
        if (arg.path != null) path = arg.path + "/" + path;
        if (arg.behavior != null) behaviorFileName = arg.behavior + ".json";
    }

    const finalPath = path + "/" + behaviorFileName;
    logs.push("Final Path: " + finalPath);

    return await jsonFileResponseScript(request, response, finalPath, logs);
}

module.exports = execute;