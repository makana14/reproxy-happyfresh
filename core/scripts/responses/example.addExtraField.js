const ReproxyResponse = require("../../../model/ReproxyResponse");

async function execute(request, response, arg) {
    if (!request.statusFeatureEnabled) {
        return nonStatusExecute(request, response, arg);
    } else {
        const responseData = response.data;
        if (responseData === null) {
            responseData = {
                extra: arg
            }
        } else if (typeof responseData === "string") {
            responseData = responseData + arg;
        } else if (typeof responseData === "object") {
            responseData.extra = arg;
        }

        response.data = responseData;

        return response;
    }
}

function nonStatusExecute(request, response, arg) {
    if (response === null) {
        response = {
            extra: arg
        }
    } else if (typeof response === "string") {
        response = response + arg;
    } else if (typeof response === "object") {
        response.extra = arg;
    }

    return response;
}

module.exports = execute;