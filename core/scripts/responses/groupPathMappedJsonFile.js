const ruleResponseCreator = require('../../../controllers/util/ruleResponseCreator')

async function execute(request, response, args, logs) {

    const responseSpec = { 
        type: "PATH_MAPPED_JSON_FILE",
        value: {
            path: args.path,
            behavior: args.behaviors[request.urlObj.pathname]
        }
    }
    
    return ruleResponseCreator.createResponse(request, responseSpec, logs, response);
}

module.exports = execute;