const fileRetriever = require('../../../controllers/util/fileRetriever')

async function execute(request, response, arg, logs) {
    const jsScript = await fileRetriever.getResponseJsScript(arg.name);
    return await jsScript(request, response, arg.argument, logs);
}

module.exports = execute;