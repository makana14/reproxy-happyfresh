const ReproxyResponse = require("../../../model/ReproxyResponse");

async function execute(request, response, arg, logs) {
    const axios = require("axios");

    // TODO: The header and URL appending should be coming from the rules config (Or original request (?))
    let finalUrl = null;
    let headers = {};

    if (typeof arg === 'string') {
        finalUrl = arg + request.url;
        // BC for old usage for mobile app
//       headers = {'Origin': 'm.api-stage.happyfresh.com', 'Accept': 'text/json'};
         //Replace headers from mobile app
         headers = request.headers;
    } else if (typeof arg === 'object') {
        finalUrl = arg.url + request.url;
        const forwardedHeaders = arg.forwardedHeaders || [];
        forwardedHeaders.forEach(headerName => {
            headers[headerName] = request.headers[headerName];
        });
    }
    
    if (!request.statusFeatureEnabled) {
        return await nonStatusExecute(request, response, arg, logs, headers, finalUrl, axios);
    }

    try {
        let axiosResponse;
        logs.push("Redirect Header: " + JSON.stringify(headers));
        logs.push("Redirect Request: " + JSON.stringify(request.body));
        if (request.method === "POST") {
            axiosResponse = await axios.post(finalUrl, {}, { headers, data: request.body});
        } else {
            axiosResponse = await axios.get(finalUrl, { headers });
        }

        return new ReproxyResponse(axiosResponse.data, axiosResponse.status);
    } catch (error) {
        logs.push("Redirect Error: " + error);
        return new ReproxyResponse(error.response.data, error.response.status);
    }
}

async function nonStatusExecute(request, response, arg, logs, headers, finalUrl, axios) {
    try {
        let response;
        logs.push("Redirect Header: " + JSON.stringify(headers));
        logs.push("Redirect Request: " + JSON.stringify(request.body));
        if (request.method === "POST") {
            response = await axios.post(finalUrl, {}, { headers, data: request.body});
        } else {
            response = await axios.get(finalUrl, { headers });
        }

        return response.data;
    } catch (error) {
        logs.push("Redirect Error: " + error);
        return error.response.data;
    }
}

module.exports = execute;