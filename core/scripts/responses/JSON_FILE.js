const fileRetriever = require('../../../controllers/util/fileRetriever')

async function execute(request, response, arg, logs) {
    if (!request.statusFeatureEnabled) {
        return await nonStatusExecute(request, response, arg, logs);
    }

    const json =  await fileRetriever.getJsonMockFile(arg);
    response.data = json;
    return response;
}

async function nonStatusExecute(request, response, arg, logs) {
    return await fileRetriever.getJsonMockFile(arg);
}


module.exports = execute;