
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function execute(request, response, arg, logs) {
    await timeout(arg)
    return response;
}

module.exports = execute;