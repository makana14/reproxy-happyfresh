async function execute(request, response, arg) {
    if (!request.statusFeatureEnabled) {
        return nonStatusExecute(request, response, arg);
    }

    response.data = arg.prefix + request.url;
    return response;
}

function nonStatusExecute(request, response, arg) {
    const prefix = arg.prefix;
    return prefix + request.url;
}

module.exports = execute;