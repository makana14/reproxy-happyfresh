const conditionMatcher = require("../../../controllers/util/conditionMatcher")

async function execute(request, arg) {
    if (Array.isArray(arg)) {
        return await isAnyOfMultipleTypes(request, arg);
    } else {
        const conditionType = arg.type;
        const values = arg.values;

        const normalizedValues = [];
        for(const val of values) {
            normalizedValues.push({"type": conditionType, "value": val});
        }

        return await isAnyOfMultipleTypes(request, normalizedValues);
    }
}

async function isAnyOfMultipleTypes(request, value) {
    for (const rule of value) {
        if (await conditionMatcher.isMatch(request, rule)) {
            return true;
        }
    }
    
    return false;
}


module.exports = execute;