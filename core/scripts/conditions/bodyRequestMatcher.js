var deepEqual = require('deep-equal')

function execute(request, arg) {
    const body = request.body;
    console.log(body);
    for (property in arg) {
        let propertyParsed = getProperty(body, property);
        let expectedValue = arg[property];
        
        console.log(propertyParsed);
        console.log(expectedValue);
        console.log("END")

        if (!deepEqual(propertyParsed, expectedValue)) {
            return false;
        }
     }

     return true;
}

// https://stackoverflow.com/a/6491615/2649132
function getProperty(obj, prop) {
    console.log("Getting: ")
    console.log(prop)
    console.log("From")
    console.log(obj)
    const parts = prop.split('.');

    if (Array.isArray(parts) && parts.length > 1) {
        let last = parts.pop(),
        l = parts.length,
        i = 1,
        current = parts[0];

        while((obj = obj[current]) && i < l) {
            current = parts[i];
            i++;
        }

        if(obj) {
            return obj[last];
        }
    } else {
        console.log("HERE")
        return obj[prop];
    }
}

module.exports = execute;