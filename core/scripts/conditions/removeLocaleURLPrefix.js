function execute(request, arg) {
    let url = getUrlWithoutLocale(request.url);
    request.updateUrl(url);
    return true;
}

function getUrlWithoutLocale(url) {
    const paths = url.split('/');
    let firstSegmentIsLocale = false;

    // todo: Should make this as modifier instead of script.
    // todo: Need to check for valid locale instead of reading pattern
    if (paths.length > 2) {
        firstSegmentIsLocale = isLocaleString(paths[0]);
    }

    return firstSegmentIsLocale ? url.substring(paths[0].length + 1) : url;
}

function isLocaleString(str) {
    const splitted = str.split('-');
    if (splitted.length === 2) {
        return splitted[0].length === 2 && splitted[1].length === 2
    }

    return false;
}

module.exports = execute;