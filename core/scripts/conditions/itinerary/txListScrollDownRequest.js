function execute(request, arg) {
    const body = request.body;
    if (body == null || body.data == null || body.data.lastId == null) return false;

    return true;
}

module.exports = execute;