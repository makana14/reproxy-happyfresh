function execute(request, arg) {
    const body = request.body;
    if (body == null || body.data == null) return false;
    if (body.data.filterStatus == "ONGOING") return true;

    return false;
}

module.exports = execute;