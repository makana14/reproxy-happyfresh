function execute(request, arg) {
    const body = request.body;
    if (body == null || body.data == null) return false;
    if (body.data.cardType != null && body.data.creditCardNumber != null) return true;

    return false;
}

module.exports = execute;
