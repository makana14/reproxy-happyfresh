function execute(request, arg) {
    const body = request.body;
    console.log(body.data)
    if (body == null || body.data == null) return false;
    if (body.data.amount != null && body.data.invoiceId != null) return true;

    return false;
}

module.exports = execute;
