function execute(request, arg) {
    return request.url === arg;
}

module.exports = execute;