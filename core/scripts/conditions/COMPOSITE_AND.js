const conditionMatcher = require("../../../controllers/util/conditionMatcher")

async function execute(request, arg) {
    for (const rule of arg) {
        if (!await conditionMatcher.isMatch(request, rule)) {
            return false;
        }
    }

    return true;
}

module.exports = execute;