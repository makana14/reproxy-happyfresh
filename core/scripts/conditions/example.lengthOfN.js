function execute(request, arg) {
    return request.url.length === arg;
}

module.exports = execute;