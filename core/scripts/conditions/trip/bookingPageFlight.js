function execute(request, arg) {
    const body = request.body;
    if (body == null || body.data == null || body.data.selectedProductSpec == null) return false;
    if (body.data.selectedProductSpec.productType == "FLIGHT") return true;

    return false;
}

module.exports = execute;