const fileRetriever = require("../../../controllers/util/fileRetriever")

async function execute(request, arg) {
    const jsScript = await fileRetriever.getConditionJsScript(arg.name);
    return await jsScript(request, arg.argument);
}

module.exports = execute;