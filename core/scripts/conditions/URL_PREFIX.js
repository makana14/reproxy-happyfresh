function execute(request, arg) {
    return request.url.startsWith(arg);
}

module.exports = execute;