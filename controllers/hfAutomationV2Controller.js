const fileRetriever = require('./util/fileRetriever')
const fs = require('fs');
const util = require('util')
const fsExtra = require('fs-extra')

const fsWriteFileAsync = util.promisify(fs.writeFile);

async function reset(req, res, ruleSetName) {
    let result = await resetInternal(ruleSetName);
    res.status(result.code).json(result.content);
}

async function resetBulk(req, res) {
    const ruleSetFiles = req.body.ruleSets;
    const failed = [];
    for(const ruleSet of ruleSetFiles) {

        try {
            let result = await resetInternal(ruleSet);
            if (result.error) {
                failed.push({
                    name: ruleSet,
                    error: result.content
                });
            }
        } catch (e) {
            console.log(e);
            failed.push({
                name: ruleSet,
                error: e
            })
        }
    }

    res.json({ failed });
}

async function resetInternal(ruleSetName) {
    let ruleSetObj = await getRuleSetFileAsync(ruleSetName);
    if (!ruleSetObj.error) {

        for (const rule of ruleSetObj.file.rules) {
            if (rule.condition.type === "URL_MATCH") {
                rule.response = {
                    "type": "JSON_FILE",
                    "value": getJsonFile(rule.condition.value, "default.json")
                }
            }
        }

        return await returnWriteFileOperation(ruleSetObj.directory, ruleSetObj.file);
    } else {
        return { code: ruleSetObj.code, content: ruleSetObj.error, error: true };
    }
}

function getJsonFile(path, fileName) {
    return "happyfresh/" + path + "/" + fileName;
}

async function changeBehavior(req, res, ruleSetName) {
    let ruleSetObj = await getRuleSetFileAsync(ruleSetName);
    let result = {};
    if (!ruleSetObj.error) {
        const newBehaviors = req.body.behaviors;

        for (const [path, behavior] of Object.entries(newBehaviors)) {
            for (const rule of ruleSetObj.file.rules) {
                if (rule.condition.type === "URL_MATCH" && rule.condition.value === path) {
                    rule.response = behavior;
                    break;
                }
            }
        }

        result = await returnWriteFileOperation(ruleSetObj.directory, ruleSetObj.file);
    } else {
        result = { code: ruleSetObj.code, content: ruleSetObj.error, error: true };
    }

    res.status(result.code).json(result.content);
}

async function createSession(req, res, masterRuleSetName, sessionId) {
    // 1. Empty the target dir
    try {
        const dirName = "./core/rulesets/happyfresh/generated" + `/${sessionId}`;
        await fsExtra.emptyDir(dirName);
    
        // 2. Copy all the contents from master to that generated folder
        const masterRuleSetDir = masterRuleSetName.replace(/\./g, "/");
        const masterDir = "./core/rulesets/" + masterRuleSetDir;
        await fsExtra.copy(masterDir, dirName);
    
        // 3. Add the "generated/{sessionId}" to the RULE_SET rules
        const newDir = `happyfresh/generated/${sessionId}`
        const oldDir = masterRuleSetDir;
        await changeRuleSetValue(`${newDir}/index.json`, oldDir, newDir, {});

        res.status(200).json({"status": "OK"});
    } catch (e) {
        res.status(500).json({"error": e})
    }
}

async function endSession(req, res, sessionId) {
    try {
        const dirName = `./core/rulesets/happyfresh/generated/${sessionId}`;
        await fsExtra.emptyDir(dirName);
        await fsExtra.rmdir(dirName);    

        res.status(200).json({"status": "OK"});
    } catch (e) {
        res.status(500).json({"error": e})
    }
}

async function changeRuleSetValue(ruleSetName, oldDir, newDir, visitedFile) {
    console.log("Trying to change: " + ruleSetName);
    
    if (visitedFile[ruleSetName] != null) {
        console.log(ruleSetName + " already on process, skipping");
        return;
    }

    visitedFile[ruleSetName] = true;

    if (ruleSetName.endsWith(".json")) {
        ruleSetName = ruleSetName.substring(0, ruleSetName.length - 5);   
    }

    console.log("FileName: " + ruleSetName);
    console.log("OldDir: " + oldDir);
    console.log("NewDir: " + newDir);

    const content = await fileRetriever.getRuleSetFile(ruleSetName);
    for (const rule of content.rules) {
        await changeRuleResponse(rule.response, oldDir, newDir, visitedFile);
    }

    await writeRuleSetFile(ruleSetName, content);
}

async function changeRuleResponse(response, oldDir, newDir, visitedFile) {
    if (response.type == "RULE_SET") {
        const oldVal = response.value;
        const newVal = oldVal.replace(oldDir, newDir);
        console.log(`oldVal: ${oldVal} | newVal: ${newVal}`);
        response.value = newVal;

        await changeRuleSetValue(response.value, oldDir, newDir, visitedFile);

    } else if (response.type == "SEQUENCE") {
        for (const res of response.value) {
            await changeRuleResponse(res, oldDir, newDir, visitedFile);
        }
    }
}

async function returnWriteFileOperation(directory, content) {
    let result = await writeRuleSetFile(directory, content);
    if (result) {
        return { code: result.code, content: result.error, error: true };
    } else {
        return { code: 200, content: { "status": "OK" }};
    }
}

async function writeRuleSetFile(ruleSetDirectory, content) {
    try {
        await fsWriteFileAsync(`./core/rulesets/${ruleSetDirectory}.json`, JSON.stringify(content, null, 2));
        return null;

    } catch (err) {
        return {
                error: { type: "ERROR_WRITING_FILE", message: 'Error: ' + err },
                code: 503
        }
    }
}

async function getRuleSetFileAsync(ruleSetName) {
    const ruleSetDirectory = ruleSetName.replace(/\./g, "/");
    const ruleSetFile = await fileRetriever.getRuleSetFile(ruleSetDirectory);

    let response = {};

    // TODO, currently the logic assumed lots of thing. Need to work on this to be more flexible.
    if (ruleSetFile != null) {
        const firstResponse = ruleSetFile.rules[0].response;
        response.file = ruleSetFile;
        response.directory = ruleSetDirectory;
        return response;
    } else {
        response.error = { type: "FILE_NOT_FOUND", message: `RuleSetName ${ruleSetDirectory} not found` };
        response.code = 404;
        return response;
    }
}

exports.reset = reset;
exports.resetBulk = resetBulk;
exports.changeBehavior = changeBehavior;
exports.createSession = createSession;
exports.endSession = endSession;