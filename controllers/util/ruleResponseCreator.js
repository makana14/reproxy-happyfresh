const ReproxyResponse = require("../../model/ReproxyResponse");
const fileRetriever = require("../util/fileRetriever");

async function createResponse(request, responseSpec, logs, currentResponse) {
    if (!request.statusFeatureEnabled) {
        return await nonStatusCreateResponse(request, responseSpec, logs, currentResponse);
    }

    if (currentResponse === null) {
        currentResponse = new ReproxyResponse();
    }

    if (!isNaN(responseSpec.status)) {
        currentResponse.status = responseSpec.status;
    }

    const jsScript = await fileRetriever.getResponseJsScript("JS_SCRIPT");
    const arg = {
        name: responseSpec.type,
        argument: responseSpec.value
    };

    currentResponse = await jsScript(request, currentResponse, arg, logs);

    if (currentResponse !== null && !(currentResponse instanceof ReproxyResponse)) {
        throw "Response not an instance of ReproxyResponse for reponseSpec " + JSON.stringify(responseSpec);
    }

    return currentResponse;
}

async function nonStatusCreateResponse(request, responseSpec, logs, currentResponse) {
    // Created for easy BC in configuration, will be deleted once status enhancement is well-tested
    const jsScript = await fileRetriever.getResponseJsScript(responseSpec.type)
    return await jsScript(request, currentResponse, responseSpec.value, logs)
}

exports.createResponse = createResponse;