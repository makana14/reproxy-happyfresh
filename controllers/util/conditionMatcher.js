const fileRetriever = require("../util/fileRetriever");

async function isMatch(request, condition) {
    const jsScript = await fileRetriever.getConditionJsScript(condition.type);
    return await jsScript(request, condition.value);
}

exports.isMatch = isMatch;