const conditionMatcher = require("../util/conditionMatcher");
const ruleResponseCreator = require("../util/ruleResponseCreator");
const fileRetriever = require("../util/fileRetriever");
const ReproxyResponse = require("../../model/ReproxyResponse");

async function createResponseFromRuleSetDirectory(request, ruleSetDirectory, logs, currentResponse = null) {
    logs.push("Using RuleSet: " + ruleSetDirectory);
    const ruleSet = await fileRetriever.getRuleSetFile(ruleSetDirectory);
    return await createResponseFromRuleSet(request, ruleSet, logs, currentResponse);
}

async function createResponseFromRuleSet(request, ruleSet, logs, currentResponse = null) {
    let matchedRule = null;
    try {
        matchedRule = await getMatchedRule(request, ruleSet, logs);
    } catch (error) {
        logs.push("RuleSet getMatchedRuleError: " + error);
        logs.push(error.trace);
        const errMessage = "RuleSet getMatchedRule Error: " + error + " (" + request.url + ")";
        if (request.statusFeatureEnabled) {
            return new ReproxyResponse(errMessage, 500);
        } else {
            return errMessage;
        }
    }

    try {
        if (matchedRule != null) {
            return await ruleResponseCreator.createResponse(request, matchedRule.response, logs, currentResponse);
        }
    } catch (error) {
        logs.push("RuleSet CreateResponseError: " + error);
        logs.push(error.trace);
        const errMessage = "RuleSet CreateResponse Error: " + error + " (" + request.url + ")";
        if (request.statusFeatureEnabled) {
            return new ReproxyResponse(errMessage, 500);
        } else {
            return errMessage;
        }
    }

    logs.push("RuleSet No Matching Rule Found");
    const notFoundMessage = "No rule found for: " + request.url;
    if (request.statusFeatureEnabled) {
        return new ReproxyResponse(notFoundMessage, 404);
    } else {
        return notFoundMessage;
    }
}

async function getMatchedRule(request, ruleSet, logs) {
    const rules = ruleSet.rules;

    for (const rule of rules) {
        if (!Boolean(rule.disable) && await conditionMatcher.isMatch(request, rule.condition)) {
            logs.push("Match Found:", JSON.stringify(rule.condition, null, 2));
            return rule;
        }
    }

    return null;
}

exports.createResponseFromRuleSetDirectory = createResponseFromRuleSetDirectory;