const RULE_SET_FOLDER_PATH = "../../core/rulesets";
const MOCK_FOLDER_PATH = "../../core/mocks";
const SCRIPT_FOLDER_PATH = "../../core/scripts";
const SCRIPT_CONDITION_FOLDER_PATH = `${SCRIPT_FOLDER_PATH}/conditions`;
const SCRIPT_RESPONSE_FOLDER_PATH = `${SCRIPT_FOLDER_PATH}/responses`;

async function getRuleSetFile(ruleSetName) {
    return await getFile(RULE_SET_FOLDER_PATH + `/${ruleSetName}`, true);
}

async function getJsonMockFile(jsonFileName) {
    return await getFile(MOCK_FOLDER_PATH + `/${jsonFileName}`, true);
}

async function getConditionJsScript(jsScriptName) {
    return await getFile(SCRIPT_CONDITION_FOLDER_PATH + `/${jsScriptName}`, false);
}

async function getResponseJsScript(jsScriptName) {
    return await getFile(SCRIPT_RESPONSE_FOLDER_PATH + `/${jsScriptName}`, false);
}

async function getFile(filePath, deleteCache) {
    try {
        if (deleteCache) {
            delete require.cache[require.resolve(filePath)]
        }
        const script = require(filePath);
        return script;
    } catch (error) {
        console.log(error);
        return null;
    }
}

exports.getRuleSetFile = getRuleSetFile;
exports.getJsonMockFile = getJsonMockFile;
exports.getConditionJsScript = getConditionJsScript;
exports.getResponseJsScript = getResponseJsScript;
exports.getFile = getFile;