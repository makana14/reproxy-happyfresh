const RULES_RUN_PATH = "rules/run";

const ruleSetResponseCreator = require("./util/ruleSetResponseCreator");

var ReproxyRequest = require("../model/ReproxyRequest");
const ReproxyResponse = require("../model/ReproxyResponse");

async function getResponse(req, res, ruleSetName) {
    const requestedUrl = getRequestedUrl(req.originalUrl, ruleSetName);  
    const ruleSetDirectory = ruleSetName.replace(/\./g, "/");

    const response = await createResponse(req, ruleSetDirectory, requestedUrl);

    if (!req.app.get('statusFeatureEnabled')) {
        return res.send(response);
    } else {
        if (response instanceof ReproxyResponse) {
            const status = response === null ? 200 : response.status;
            const data = response === null ? null : response.data;

            return res.status(status).send(data);
        } else {
            return res.status(200).send(response);
        }
    }
}

// Extract the requestedUrl from originalUrl
function getRequestedUrl(originalUrl, rulesName) {
    const fullPath = RULES_RUN_PATH + "/" + rulesName + "/";
    const startIndex = originalUrl.indexOf(fullPath);
    return originalUrl.substring(startIndex + fullPath.length);
}

async function createResponse(req, ruleSetDirectory, requestedUrl) {
    let request = new ReproxyRequest(requestedUrl, req.method, req.body, req.headers, req.app.get('statusFeatureEnabled'));
    let logs = [];
    logs.push("=========START=========");
    logs.push(request)
    let response = await ruleSetResponseCreator.createResponseFromRuleSetDirectory(request, ruleSetDirectory, logs);
    logs.push("=========END=========");

    console.log(logs)
    console.log(logs.join("\n"));
    
    return response;
}

exports.getResponse = getResponse;