# reproxy

## What
NodeJs server that will gives response based on customizable defined rules.

## How to use
1. Clone the project
2. Go into the directory where the project is cloned
3. Install Node.js & npm [Tutorial](http://blog.teamtreehouse.com/install-node-js-npm-mac).
4. run `npm install`
5. install nodemon: `npm install -g nodemon`
6. run `nodemon ./bin/www`

## Contributions
Any contribution accepted! :joy: