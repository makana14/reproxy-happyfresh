class ReproxyResponse {
    
    constructor(data = null, status = 200) {
        this.data = data;
        this.status = status;
    }
}


module.exports = ReproxyResponse;