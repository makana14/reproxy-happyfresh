const urlParser = require('url');

class ReproxyRequest {

    constructor(url, method, body, headers, statusFeatureEnabled) {
        this.method = method;
        this.body = body;
        this.headers = headers;
        this.updateUrl(url);
        this.statusFeatureEnabled = statusFeatureEnabled;
    }

    updateUrl(url) {
        this.url = url;
        this.urlObj = urlParser.parse(url);
    }
}


module.exports = ReproxyRequest;